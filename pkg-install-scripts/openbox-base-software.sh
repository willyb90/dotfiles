#!/bin/bash
set -e
##################################################################################################################
# Author 	: WillyB
# Enviroment : openbox
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#software from Arch Linux repositories

sudo pacman -S --noconfirm --needed xdg-user-dirs
sudo pacman -S --noconfirm --needed xdg-utils
sudo pacman -S --noconfirm --needed dbus
sudo pacman -S --noconfirm --needed lxsession
sudo pacman -S --noconfirm --needed lightdm-gtk-greeter-settings
sudo pacman -S --noconfirm --needed firefox
sudo pacman -S --noconfirm --needed thunderbird
sudo pacman -S --noconfirm --needed base-devel
sudo pacman -S --noconfirm --needed thunar
sudo pacman -S --noconfirm --needed thunar-archive-plugin
sudo pacman -S --noconfirm --needed thunar-media-tags-plugin
sudo pacman -S --noconfirm --needed thunar-volman
sudo pacman -S --noconfirm --needed gvfs
sudo pacman -S --noconfirm --needed git
sudo pacman -S --noconfirm --needed terminator
sudo pacman -S --noconfirm --needed geany
sudo pacman -S --noconfirm --needed tint2
sudo pacman -S --noconfirm --needed compton
sudo pacman -S --noconfirm --needed obconf
sudo pacman -S --noconfirm --needed lxappearance
sudo pacman -S --noconfirm --needed lxappearance-obconf
sudo pacman -S --noconfirm --needed ttf-liberation
sudo pacman -S --noconfirm --needed ttf-dejavu
sudo pacman -S --noconfirm --needed archlinux-wallpaper
sudo pacman -S --noconfirm --needed arc-gtk-theme
sudo pacman -S --noconfirm --needed unzip
sudo pacman -S --noconfirm --needed unrar
sudo pacman -S --noconfirm --needed xarchiver
sudo pacman -S --noconfirm --needed htop
sudo pacman -S --noconfirm --needed neofetch
sudo pacman -S --noconfirm --needed bash-completion
sudo pacman -S --noconfirm --needed rofi
sudo pacman -S --noconfirm --needed cronie
sudo pacman -S --noconfirm --needed pulseaudio
sudo pacman -S --noconfirm --needed pulseaudio-alsa
sudo pacman -S --noconfirm --needed pavucontrol
sudo pacman -S --noconfirm --needed volumeicon
sudo pacman -S --noconfirm --needed dunst
sudo pacman -S --noconfirm --needed vlc
sudo pacman -S --noconfirm --needed tumbler
sudo pacman -S --noconfirm --needed feh
sudo pacman -S --noconfirm --needed nss-mdns
sudo pacman -S --noconfirm --needed cups
sudo pacman -S --noconfirm --needed gutenprint
sudo pacman -S --noconfirm --needed foomatic-db-gutenprint-ppds
sudo pacman -S --noconfirm --needed mesa
sudo pacman -S --noconfirm --needed xf86-video-amdgpu
sudo pacman -S --noconfirm --needed glu
sudo pacman -S --noconfirm --needed xsel
sudo pacman -S --noconfirm --needed xclip
sudo pacman -S --noconfirm --needed meld
sudo pacman -S --noconfirm --needed okular
sudo pacman -S --noconfirm --needed ristretto

###############################################################################################

echo "################################################################"
echo "################### core software installed"
echo "################################################################"

